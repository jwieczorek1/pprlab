package com.noSkill;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;


public class Main {

    public static void main(String[] args) {

        int tConst = 51264123;
        int n= 100000;
        double[] zTable = generateZTable(n);
        calculateWiernerSequential(zTable, n, tConst);
        List<Double> zList = generateZList(zTable);
        calculateWiernerInThread(zList, n, tConst);

    }

    /**
     * calculating wierner sequentially, return value on standard output. writes also how long does calculation takes
     * @param zTable array of Z values
     * @param n count of elems
     * @param t t const param
     */
    private static void calculateWiernerSequential(double[] zTable, int n, int t) {

        long start = System.currentTimeMillis();
        double sum = 0.0;
        for (double z: zTable) {
            sum += z * (Math.sin((n-0.05)*Math.PI*t)/((n-0.5)*Math.PI));
        }

        System.out.println(Math.pow(2, 2) * sum );
        System.out.println("Takes " + (System.currentTimeMillis() - start) + " ms");
    }

    /**
     * calculating wierner using threads
     * @param zList list of Z values
     * @param n count of elems
     * @param t t const param
     */
    private static void calculateWiernerInThread(List<Double> zList, int n, int t) {

        long start = System.currentTimeMillis();
        double calc = zList.parallelStream().collect(Collectors.summarizingDouble(z ->
                    (z * (Math.sin((n - 0.05) * Math.PI * t) / ((n - 0.5) * Math.PI)))
        )).getSum();
        System.out.println(calc);
        System.out.println("Takes " + (System.currentTimeMillis() - start) + " ms");

    }

    /**
     * generates table of Z values
     * @param n count of elems
     * @return zTable
     */
    private static double[] generateZTable(int n) {
        Random random = new Random();

        double[] zTable = new double[n];

        for (int i = 0; i < n; i++) {
            zTable[i] =  calculateZn(random.nextDouble(), random.nextDouble());
        }

        return zTable;
    }

    private static double calculateZn(double x, double y){

        return Math.pow(-2 * Math.log(x) * Math.sin(2*Math.PI*y) ,2);
    }

    private static List<Double> generateZList(double[] zTable) {
        List<Double> zList = new ArrayList<>();
        for (double z : zTable) {
            zList.add(z);
        }
        return zList;
    }
}

